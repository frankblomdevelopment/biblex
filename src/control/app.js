import Vue from "vue";
import Control from "./Control.vue";

import "@/assets/admin.css";
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(Control),
}).$mount("#app");
