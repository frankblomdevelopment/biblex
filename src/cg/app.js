import Vue from "vue";
import CG from "./CG";

import "@/assets/cg.css";

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(CG),
}).$mount("#app");
