import Vue from "vue";
import Admin from "./Admin.vue";
import form from "../form.js";

import "@/assets/admin.css";

Vue.config.productionTip = false;

Vue.use(form);

new Vue({
  render: (h) => h(Admin),
}).$mount("#app");
