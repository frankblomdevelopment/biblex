import Vue from "vue";

function buildForm(fields) {
  return Vue.observable({
    ...fields,
    data() {
      return Object.keys(fields).reduce((carry, key) => {
        carry[key] = this[key];
        return carry;
      }, {});
    },
  });
}

export default {
  install(Vue) {
    Vue.prototype.$form = (data) => buildForm(data);
  },
};
