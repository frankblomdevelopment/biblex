const Module = require("./Module");
const uuid = require("uuid");

class Question extends Module {
  constructor(app) {
    super(app);
    this.State.default("questions", []);
    return this;
  }
  _sendUpdate() {
    this._broadcast("questions.updated", this._list());
  }

  _list() {
    return this.State.get("questions").sort((a, b) => a.order - b.order);
  }

  _exists(id) {
    return !id ? false : this.State.get("questions").some((x) => x.id == id);
  }

  newOrder() {
    const list = this._list();
    return list.length ? list[list.length - 1].order + 1 : 1;
  }

  getNext(id) {
    const list = this._list();
    const nextIndex = list.findIndex((x) => x.id == id) + 1;
    const index = nextIndex < list.length ? nextIndex : 0;

    return list[index];
  }

  setActive(id) {
    const current = this.State.get("questions").findIndex((x) => x.active);

    if (current > -1) {
      this.State.set(`questions.${current}.active`, false);
    }

    if (id) {
      const index = this._getPath(id);
      this.State.set(`questions.${index}.active`, true);
    }

    this._sendUpdate();
  }

  getPrev(id) {
    const list = this._list();
    const newIndex = list.findIndex((x) => x.id == id) - 1;
    const index = newIndex >= 0 ? newIndex : list.length - 1;

    return list[index];
  }

  add(data) {
    this.State.add("questions", {
      ...data,
      id: uuid.v4(),
      order: this.newOrder(),
      active: false,
    });
    this._sendUpdate();
  }

  _getPath(id) {
    if (!id) return false;
    const index = this.State.get("questions").findIndex((x) => x.id == id);

    return this.State.has(`questions.${index}`) ? index : false;
  }

  update({ id, data }) {
    const index = this._getPath(id);
    if (index < 0) return;

    this.State.set(`questions.${index}`, data);
    this._sendUpdate();
  }

  destroy(id) {
    const questions = this.State.get("questions").filter((x) => x.id !== id);
    this.State.set("questions", questions);

    this._sendUpdate();
  }

  order({ id, direction }) {
    const index = this._getPath(id);
    if (index < 0) return;

    const question = this.State.get(`questions.${index}`);

    const swapwith =
      direction == "up" ? this.getPrev(question.id) : this.getNext(question.id);

    const swapQuestionIndex = this._getPath(swapwith.id);

    const oldOrder = question.order;
    const newOrder = swapwith.order;

    this.State.set(`questions.${swapQuestionIndex}.order`, oldOrder);
    this.State.set(`questions.${index}.order`, newOrder);

    this._sendUpdate();
  }

  _updateSetting(path, value) {}
}

module.exports = Question;
