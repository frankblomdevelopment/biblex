const QuestionModule = require("./Question");
const CGModule = require("./CG");

module.exports = [QuestionModule, CGModule];
