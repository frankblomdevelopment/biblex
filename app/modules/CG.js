const Module = require("./Module");

class CG extends Module {
  constructor(app) {
    super(app);
    this.State.default("CG", {
      running: false,
      current: "", // the id of the current item,
      timer: 0,
    });

    return this;
  }
  _sendUpdate() {
    this._broadcast("CG", this.State.get("CG"));
  }

  start() {
    //find the next question
    const CG = this.State.get("CG");
    const question = this.App.modules["question"].getNext(CG.current);

    // there are no questions
    if (!question) {
      return;
    }

    this.State.set("CG", {
      running: true,
      current: question.id,
      timer: 15,
    });

    this.App.modules["question"].setActive(question.id);
    this._sendUpdate();
  }

  tick() {
    const CG = this.State.get("CG");
    if (!CG.running) return;

    if (CG.timer <= 0) {
      return this.start();
    }

    this.State.set("CG.timer", CG.timer - 1);
    this._sendUpdate();
  }

  select(id) {
    if (!this.App.modules["question"]._exists(id)) {
      return;
    }
    this.State.set("CG.current", id);
    this.State.set("CG.timer", 15);
    this.App.modules["question"].setActive(id);

    this._sendUpdate();
  }

  stop() {
    this.State.set("CG.running", false);
    this.App.modules["question"].setActive(false);

    this._sendUpdate();
  }

  _updateSetting(path, value) {}
}

module.exports = CG;
