class Module {
  constructor(App) {
    this.App = App;
    this.State = App.State;
  }
  _broadcast(key, data) {
    this.App.broadcast(key, data);
  }
}

module.exports = Module;
