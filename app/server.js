const host = require("./services/host-service");
const App = require("./App");
const { splitMessage } = require("./support.js");

const Websocket = require("socket.io")(host.server, {
  cors: { origin: "*", methods: "GET" },
});

const BibelXQA = new App();

Websocket.on("connection", (socket) => {
  socket.emit("state", BibelXQA.getState());
  socket.use((data) => {
    const path = splitMessage(data.shift(), ".");
    BibelXQA.onMessage(path[0], path[1], data.flat(2));
  });
});

BibelXQA.on("broadcast", ({ key, value }) => {
  Websocket.emit(key, value);
});

host.app.get("/start", (req, res) => {
  BibelXQA.show();
  res.send("OK!");
});

host.app.get("/stop", (req, res) => {
  BibelXQA.hide();
  res.send("OK!");
});

const port = process.argv.slice(2)[0] || 80;

host.server.listen(port);
console.log(`Started server on port ${port}`);
