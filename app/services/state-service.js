const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

class State {
  constructor() {
    if (!!State.instance) {
      return State.instance;
    }
    State.instance = this;
    this.db = low(new FileSync("./state.json"));

    return this;
  }

  default(path, def) {
    if (this.has(path)) return;
    this.set(path, def);
  }

  has(path) {
    return this.db.has(path).value();
  }

  get(path) {
    return this.db.get(path).value();
  }

  set(path, value) {
    return this.db.set(path, value).write();
  }

  update(path, value) {
    return this.db.update(path, value).write();
  }

  remove(path, value) {
    return this.db.get(path).pull(value).write();
  }

  add(path, value) {
    return this.db.get(path).push(value).write();
  }

  unset(path) {
    return this.db.unset(path).write();
  }
  getFullState() {
    return this.db.getState();
  }
}

module.exports = State;
