const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);

const templatePath = path.join(process.cwd(), "public");

app.use(express.static(templatePath));

module.exports = { server, app };
