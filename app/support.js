"use strict";

module.exports = {
  splitMessage: (message, key) => {
    const path = message.split(key);
    if (path[0] === "") {
      path.shift();
    }
    return [path.shift(), path.join("")];
  },
};
