const EventEmitter = require("events");
const StateService = require("./services/state-service");
const modules = require("./modules");

class App extends EventEmitter {
  constructor(settings) {
    super();

    this.State = new StateService();

    // load in all modules
    this.modules = modules.reduce((d, m) => {
      d[m.name.toLowerCase()] = new m(this);
      return d;
    }, {});
  }

  getState() {
    return this.State.getFullState();
  }

  broadcast(key, value) {
    this.emit("broadcast", { key, value });
  }

  show() {
    this.modules["cg"].start();
  }

  hide() {
    this.modules["cg"].stop();
  }

  onMessage(module, action, data) {
    if (!module || !action) return;

    if (!this.modules[module]) return;

    if (typeof this.modules[module][action] == "function") {
      if (Array.isArray(data)) {
        this.modules[module][action](...data);
      } else if (data) {
        this.modules[module][action](data);
      } else {
        this.modules[module][action]();
      }
    } else {
      console.log(`missed Module: ${module} action:${action}`);
    }
  }
}

module.exports = App;
