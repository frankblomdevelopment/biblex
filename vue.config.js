module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "./" : "/",
  pages: {
    admin: {
      entry: "src/admin/app.js",
      template: "templates/admin.html",
      filename: "index.html",
      title: "BibelXQA - Admin",
      chunks: ["chunk-vendors", "chunk-common", "admin"],
    },
    control: {
      entry: "src/control/app.js",
      template: "templates/admin.html",
      filename: "control.html",
      title: "BibelXQA - Control",
      chunks: ["chunk-vendors", "chunk-common", "control"],
    },
    cg: {
      entry: "src/cg/app.js",
      template: "templates/cg.html",
      filename: "cg.html",
      title: "BibelXQA - CG",
      chunks: ["chunk-vendors", "chunk-common", "cg"],
    },
  },
};
